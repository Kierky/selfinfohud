custominfo = {}

--[[
Some code credit must go to Pizzasgood, modified by Kierky.
Perhaps even a TempKOS timer till the end.
--]]

custominfo.version = "1.2.3"
custominfo.init = {}
custominfo.gui = {}
custominfo.gui.pks = {}
custominfo.gui.pks.box = nil

local function fake_iup_insert(ih, ref_child, new_child)
	--first add the new stuff
	iup.Append(ih, new_child)

	--now move to the end all the old stuff from ref_child on that is not new_child
	local last_child = ref_child
	local next_child = nil
	while true do
		if last_child ~= new_child then
			next_child = iup.GetNextChild(ih, last_child)
			iup.Detach(last_child)
			iup.Append(ih, last_child)
			last_child = next_child
		else
			break
		end
	end

	--and refresh it
	iup.Refresh(ih)
end

function custominfo:update_own_stats()
	if self.gui.pks.box ~= nil then
		self:insert_pks()
	end
end

function custominfo:get_pks()
	local _,_,pks = GetCharacterKillDeaths()
	return pks
end

function custominfo:get_kills()
	local kills,_,_ = GetCharacterKillDeaths()
	return kills
end

function custominfo:get_system()
	local system = tostring(SystemNames[GetCurrentSystemid()])
	return system
end

function custominfo:get_faction_info(factionid)
	local faction = factionid
	local faction_score = GetPlayerFactionStanding(faction)
	local easyscore = math.ceil((faction_score - 32768) / 32767.9 * 1000)
	return easyscore
end

function custominfo:get_surrounding_factionids()
	local systemid = GetCurrentSystemid()
	local factionid = {}
	if systemid >0 and systemid < 31 then
		if (systemid > 0 and systemid < 3) or (systemid > 26 and systemid < 31) then
			factionid[1] = 2
		elseif systemid > 3 and systemid < 14 then
			factionid[1] = 1
		elseif systemid == 14 then
			factionid[1] = 10
		elseif systemid == 15 then
			factionid[1] = 3
			factionid[2] = 8
			factionid[3] = 6
		elseif systemid == 16 then
			factionid[1] = 12
		elseif systemid == 17 then
			factionid[1] = 9
			factionid[2] = 12
		elseif systemid == 18 then
			factionid[1] = 4
			factionid[2] = 5
			factionid[3] = 6
		elseif systemid == 19 then
			factionid[1] = 3
			factionid[2] = 4
			factionid[3] = 5
			factionid[4] = 6
			factionid[5] = 7
			factionid[6] = 8
		elseif systemid == 20 then
			factionid[1] = 9
			factionid[2] = 13
		elseif systemid == 21 then
			factionid[1] = 3
			factionid[2] = 4
			factionid[3] = 7
		elseif systemid == 22 then
			factionid[1] = 9
			factionid[2] = 10
			factionid[3] = 12
		elseif systemid == 23 then
			factionid[1] = 13
			factionid[2] = 4
			factionid[3] = 7
		elseif systemid == 24 then
			factionid[1] = 3
			factionid[2] = 4
			factionid[3] = 8
		elseif systemid == 25 then
			factionid[1] = 13
			factionid[2] = 3
			factionid[3] = 11
		elseif systemid == 26 then
			factionid[1] = 2
			factionid[2] = 11
		end
 	end
	return factionid
end

function custominfo:OnEvent(e)
	if e == "HUD_SHOW" then
		self:init()
	elseif e == "PLAYER_LOGGED_OUT" then
		self:remove_pks()
	elseif e == "UPDATE_CHARINFO" or e == "TARGET_CHANGED" or e == "PLAYER_STATS_UPDATED" or "SECTOR_CHANGED" then
		self:update_own_stats()
	end
end
-- Add the PK's to the selfinfo box
function custominfo:insert_pks()
	-- Destroy existing element that may already be there
	self:remove_pks()
	-- essentially iup.GetBrother(iup.GetBrother(iup.GetNextChild(HUD.selfinfo))) for reference
	local parent = HUD.selfinfo
	local first_child = iup.GetNextChild(parent)
	local reference = iup.GetBrother(iup.GetBrother(first_child))
	local title_font = first_child.font
	local pk_font = first_child.font
	local title_color = first_child.fgcolor
	local pk_color = iup.GetBrother(first_child).fgcolor
	local factionsurround = self:get_surrounding_factionids()
	local space = " "
	if not factionsurround then return end
	-- Title
	self.gui.pks.title = iup.label { title = "PKS / Kills", font = title_font, fgcolor = title_color, expand = "HORIZONTAL", alignment = "ALEFT" } 
	-- Value
	self.gui.pks.value = iup.label { title = ' '..comma_value(self:get_pks()).. ' / '..comma_value(self:get_kills()), font = pk_font, fgcolor = pk_color, expand = "HORIZONTAL", alignment = "ALEFT" }
	-- Title for Factions
	self.gui.pks.factiontitle = iup.label { title = 'Factions in '..self:get_system(), font = title_font, fgcolor = title_color, expand = "HORIZONTAL", alignment = "ALEFT" } 
	-- Value for standings
	-- to make it less messy on the HUD
	if #factionsurround == 1 then 
		self.gui.pks.faction1 = iup.label { title = ' '..FactionName[factionsurround[1]]..': '..self:get_faction_info(factionsurround[1]), font = pk_font, fgcolor = pk_color, expand = "HORIZONTAL", alignment = "ALEFT" }
	elseif #factionsurround > 1 then
		self.gui.pks.faction1 = iup.label { title = ' '..FactionName[factionsurround[1]]..': '..self:get_faction_info(factionsurround[1])..'\n '..FactionName[factionsurround[2]]..': '..self:get_faction_info(factionsurround[2]), font = pk_font, fgcolor = pk_color, expand = "HORIZONTAL", alignment = "ALEFT" }
	end
	if #factionsurround == 3 then 
		self.gui.pks.faction2 = iup.label { title = ' '..FactionName[factionsurround[3]]..': '..self:get_faction_info(factionsurround[3]), font = pk_font, fgcolor = pk_color, expand = "HORIZONTAL", alignment = "ALEFT" }
	elseif #factionsurround > 3 then
		self.gui.pks.faction2 = iup.label { title = ' '..FactionName[factionsurround[3]]..': '..self:get_faction_info(factionsurround[3])..'\n '..FactionName[factionsurround[4]]..': '..self:get_faction_info(factionsurround[4]), font = pk_font, fgcolor = pk_color, expand = "HORIZONTAL", alignment = "ALEFT" }
	end
	if #factionsurround == 5 then 
		self.gui.pks.faction3 = iup.label { title = ' '..FactionName[factionsurround[5]]..': '..self:get_faction_info(factionsurround[5]), font = pk_font, fgcolor = pk_color, expand = "HORIZONTAL", alignment = "ALEFT" }
	elseif #factionsurround == 6 then
		self.gui.pks.faction3 = iup.label { title = ' '..FactionName[factionsurround[5]]..': '..self:get_faction_info(factionsurround[5])..'\n '..FactionName[factionsurround[6]]..': '..self:get_faction_info(factionsurround[6]), font = pk_font, fgcolor = pk_color, expand = "HORIZONTAL", alignment = "ALEFT" }
	end


	-- Container
	if #factionsurround == 2 or #factionsurround == 1 then
		self.gui.pks.box = iup.vbox {
			self.gui.pks.title,
			self.gui.pks.value,
			self.gui.pks.factiontitle,
			self.gui.pks.faction1;
			alignment = "ALEFT"
		}
	elseif #factionsurround == 4 or #factionsurround == 3 then
		self.gui.pks.box = iup.vbox {
			self.gui.pks.title,
			self.gui.pks.value,
			self.gui.pks.factiontitle,
			self.gui.pks.faction1,
			self.gui.pks.faction2;
			alignment = "ALEFT" 
		}
	else 
		self.gui.pks.box = iup.vbox {
			self.gui.pks.title,
			self.gui.pks.value,
			self.gui.pks.factiontitle,
			self.gui.pks.faction1,
			self.gui.pks.faction2,
			self.gui.pks.faction3;
			alignment = "ALEFT" 
		}
	end
	-- Add to HUD	
	fake_iup_insert(parent, reference, self.gui.pks.box)
	self.gui.pks.box:show()

	-- Refresh
	iup.Refresh(parent)
end

-- remove display from selfinfo box
function custominfo:remove_pks()
	if self.gui.pks.box ~= nil then
		iup.Destroy(self.gui.pks.box)
		self.gui.pks.box = nil
	end
end

function custominfo:init()
	self:insert_pks()
	UnregisterEvent(custominfo, "HUD_SHOW")
end

RegisterEvent(custominfo, "PLAYER_UPDATE_FACTIONSTANDINGS")
RegisterEvent(custominfo, "SECTOR_CHANGED")
RegisterEvent(custominfo, "UPDATE_CHARINFO")
RegisterEvent(custominfo, "PLAYER_STATS_UPDATED")
RegisterEvent(custominfo, "TARGET_CHANGED")
RegisterEvent(custominfo, "HUD_SHOW")
RegisterEvent(custominfo, "PLAYER_LOGGED_OUT")